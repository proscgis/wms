package com.pr.wms.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.pr.wms.dao.MessageDao;
import com.pr.wms.dao.ReplyDao;
import com.pr.wms.model.Article;
import com.pr.wms.model.Message;
import com.pr.wms.model.Reply;

@Service("wechatService")
public class WeChatService {

	@Resource(name = "messageDao")
	private MessageDao messageDao;

	@Resource(name = "replyDao")
	private ReplyDao replyDao;

	/**
	 * 新增Message对象到数据库中
	 */
	public void addMessage(Message message) {
		messageDao.addMessage(message);
	}

	/**
	 * 将数据库中Message数据分页查出
	 * 
	 * @param start
	 *            其实数据条数
	 * @param size
	 *            展示数据每页的大小
	 */
	public List<Message> listMessage(int start, int size) {
		return messageDao.findMessage(start, size);
	}

	/**
	 * 将数据库中Message数据分页查出
	 * 
	 * @param start
	 *            其实数据条数
	 * @param size
	 *            展示数据每页的大小
	 */
	public List<Reply> listReply(int start, int size) {
		return replyDao.findReply(start, size);
	}

	/**
	 * 保存回复消息至数据库中，如果为news类型消息将article一并保存
	 */
	public void addReply(Reply reply) {
		replyDao.addReply(reply);
		if (Reply.NEWS.equals(reply.getMsgType()) && null != reply.getArticles()) {
			List<Article> articles = reply.getArticles();
			for (Article a : articles) {
				a.setReplyId(reply.getId());
				replyDao.addArticle(a);
			}
		}
	}

}
