package com.pr.wms.exception;

public class WechatException extends RuntimeException {

	public WechatException(int errcode, String errmsg) {
		super("errcode:" + errcode + ",errmsg:" + errmsg);
	}

	public WechatException(int errcode, String errmsg, Throwable cause) {
		super("errcode:" + errcode + ",errmsg:" + errmsg, cause);
	}

}
