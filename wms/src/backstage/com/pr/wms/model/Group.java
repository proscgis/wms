package com.pr.wms.model;

import java.io.Serializable;
import java.util.List;

/**
 * 分组
 */
public class Group implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	// 组名
	private String name;
	private List<Product> products;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProduct(List<Product> products) {
		this.products = products;
	}

	public String toJson() {
		StringBuilder sb = new StringBuilder("{\"group_detail\" : {\"group_name\": \"" + name + "\",\"product_list\" : [ ");
		for (Product p : products) {
			sb.append(" \"" + p.getId() + "\",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("]}}");
		return sb.toString();
	}
}
