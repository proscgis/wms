package com.pr.wms.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商品属性列表
 */
public class Properties implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	// 组名
	private String name;
	private List<Property> propertyValue;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Property> getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(List<Property> propertyValue) {
		this.propertyValue = propertyValue;
	}

	public List<Map> toWechatJsonMap() {
		List<Map> propertyList = new ArrayList<Map>();
		for (Property p : propertyValue) {
			propertyList.add(p.toWechatJsonMap());
		}
		return propertyList;
	}
}
