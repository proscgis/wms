package com.pr.wms.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 商品属性
 */
public class Property implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private String vid;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVid() {
		return vid;
	}

	public void setVid(String vid) {
		this.vid = vid;
	}

	public Map toWechatJsonMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", id);
		map.put("vid", vid);
		return map;
	}

}
