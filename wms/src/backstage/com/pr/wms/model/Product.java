package com.pr.wms.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pr.wms.util.GsonUtils;

/**
 * 商品
 */
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	// 商品名称
	private String name;
	// 商品分类id，商品分类列表请通过《获取指定分类的所有子分类》获取
	private int categoryId;
	private String categoryName;
	// 商品主图(图片需调用图片上传接口获得图片Url填写至此，否则无法添加商品。图片分辨率推荐尺寸为640×600)
	private String mainImg;
	// 商品图片列表,用；分割(图片需调用图片上传接口获得图片Url填写至此，否则无法添加商品。图片分辨率推荐尺寸为640×600)
	private String img;
	// 商品详情列表，显示在客户端的商品详情页内:文字描述
	private String detailText;
	// 商品详情列表，显示在客户端的商品详情页内:图片(图片需调用图片上传接口获得图片Url填写至此，否则无法添加商品)
	private String detailImg;
	// 商品分类id，商品分类列表请通过《获取指定分类的所有子分类》获取
	private Properties properties;
	private List<Sku> skuInfo;
	// 用户商品限购数量
	private int buyLimit;
	// 运费信息
	private Delivery delivery;
	private Timestamp createTime;
	private Timestamp updateTime;
	


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getMainImg() {
		return mainImg;
	}

	public void setMainImg(String mainImg) {
		this.mainImg = mainImg;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getDetailText() {
		return detailText;
	}

	public void setDetailText(String detailText) {
		this.detailText = detailText;
	}

	public String getDetailImg() {
		return detailImg;
	}

	public void setDetailImg(String detailImg) {
		this.detailImg = detailImg;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public List<Sku> getSkuInfo() {
		return skuInfo;
	}

	public void setSkuInfo(List<Sku> skuInfo) {
		this.skuInfo = skuInfo;
	}

	public int getBuyLimit() {
		return buyLimit;
	}

	public void setBuyLimit(int buyLimit) {
		this.buyLimit = buyLimit;
	}

	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	public Map<String, Object> toWechatJsonMap() {
		Map<String, Object> json = new HashMap<String, Object>();
		Map<String, Object> productBaseMap = new HashMap<String, Object>();
		productBaseMap.put("category_id", new String[] { String.valueOf(categoryId) });
		productBaseMap.put("property", properties.toWechatJsonMap());
		productBaseMap.put("name", name);

		List<Map<String, Object>> skuList = new ArrayList<Map<String, Object>>();
		Map<String, List<String>> skuInfoMap = new HashMap<String, List<String>>();
		for (Sku sku : skuInfo) {
			// $颜色:$白色;$尺码:$37码 | $颜色:$白色;$尺码:$38码
			// {id:$颜色,vid:[$白色,$红色]} | {id:$尺码,vid:[$37码,$38码]}
			String name = sku.getName();
			String[] skus = name.split(";");
			for (String s : skus) {
				String[] ids = s.split(":");
				String id = ids[0];
				String vid = ids[1];
				List<String> vids = skuInfoMap.get(id);
				if (vids == null) {
					vids = new ArrayList<String>();
					skuInfoMap.put(id, vids);
				}
				vids.add(vid);
			}
			skuList.add(sku.toWechatJsonMap());
		}
		List<Map<String, Object>> skuInfoList = new ArrayList<Map<String, Object>>();
		for (Map.Entry<String, List<String>> e : skuInfoMap.entrySet()) {
			Map<String, Object> sm = new HashMap<String, Object>();
			sm.put("id", e.getKey());
			sm.put("vid", e.getValue());
			skuInfoList.add(sm);
		}
		productBaseMap.put("sku_info", skuInfoList);
		productBaseMap.put("main_img", mainImg);
		productBaseMap.put("img", new String[] { img });
		List<Map<String, Object>> detailList = new ArrayList<Map<String, Object>>();
		Map<String, Object> detailMap = new HashMap<String, Object>();
		detailMap.put("text", detailText);
		detailMap.put("img", detailImg);
		detailList.add(detailMap);
		productBaseMap.put("detail", detailList);
		productBaseMap.put("buy_limit", buyLimit);
		json.put("sku_list", skuList);
		json.put("attrext", GsonUtils.fromJson("{  \"location\": { \"country\": \"中国\",  \"province\": \"山东省\",\"city\": \"济南市\",\"address\": \"小区\"},"
				+ "\"isPostFree\": 0, \"isHasReceipt\": 1,  \"isUnderGuaranty\": 0,\"isSupportReplace\": 0  }"));
		json.put("delivery_info", delivery.toWechatJsonMap());
		json.put("product_base", productBaseMap);
		return json;
	}

	public static void main(String[] args) {
		Product p = new Product();
		p.setName("白糖");
		p.setBuyLimit(10);
		p.setCategoryId(1111);
		p.setDetailText("白糖天啊");
		Properties prop = new Properties();
		prop.setName("颜色");
		List<Property> propertyValue = new ArrayList<Property>();
		Property e = new Property();
		e.setId("1");
		e.setVid("1");
		propertyValue.add(e);
		prop.setPropertyValue(propertyValue);
		p.setProperties(prop);
		List<Sku> skuInfoList = new ArrayList<Sku>();
		Sku sku1 = new Sku();
		sku1.setCateId(1111);
		sku1.setIconUrl("/jdsjdsj/jsj.jpg");
		sku1.setName("$颜色:$白色;$尺码:$37码");
		sku1.setPrice(12);
		sku1.setOriPrice(12);
		sku1.setProductCode("11");
		sku1.setQuantity(1);
		skuInfoList.add(sku1);
		Sku sku2 = new Sku();
		sku2.setCateId(1111);
		sku2.setIconUrl("/jdsjdsj/jsj.jpg");
		sku2.setName("$颜色:$白色;$尺码:$38码");
		sku2.setPrice(12);
		sku2.setOriPrice(12);
		sku2.setProductCode("11");
		sku2.setQuantity(1);
		skuInfoList.add(sku2);
		p.setSkuInfo(skuInfoList);
		Delivery deli = new Delivery();
		deli.setDeliveryType("dssd");
		deli.setExpressId("dsds");
		p.setDelivery(deli);
		System.out.println(GsonUtils.toJson(p.toWechatJsonMap()));
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
