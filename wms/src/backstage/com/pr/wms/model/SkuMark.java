package com.pr.wms.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 规格
 */
public class SkuMark implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	// 大分类ID(根节点分类id为1)
	private int cateId;
	// 规格:每个sku信息串即为一个确定的商品，比如白色的37码的鞋子：$颜色:$白色;$尺码:$37码
	private String name;
	// sku原价(单位 : 分)
	private int oriPrice;
	// sku微信价(单位 : 分, 微信价必须比原价小, 否则添加商品失败)
	private int price;
	// sku iconurl(图片需调用图片上传接口获得图片Url)
	private String iconUrl;
	// sku库存
	private int quantity;
	// 商家商品编码
	private String productCode;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCateId() {
		return cateId;
	}

	public void setCateId(int cateId) {
		this.cateId = cateId;
	}

	/**
	 * 规格:每个sku信息串即为一个确定的商品，比如白色的37码的鞋子：$颜色:$白色;$尺码:$37码
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getOriPrice() {
		return oriPrice;
	}

	public void setOriPrice(int oriPrice) {
		this.oriPrice = oriPrice;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Map<String, Object> toWechatJsonMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sku_id", name);
		map.put("price", price);
		map.put("icon_url", iconUrl);
		map.put("product_code", productCode);
		map.put("ori_price", oriPrice);
		map.put("quantity", quantity);
		return map;
	}
}
