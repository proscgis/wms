package com.pr.wms.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * 订单
 */
public class Order implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	// 送货地址
	private String address;
	//送货时间
	private Timestamp deliveryTime;
	private Timestamp createtime;
	// 购物清单
	private List<Goods> goods;
}
