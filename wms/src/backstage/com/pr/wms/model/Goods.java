package com.pr.wms.model;

import java.io.Serializable;

/**
 * 购物
 */
public class Goods implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	// 货物id
	private String merchandiseId;
	// 数量
	private int quantity;
	private String remark;

}
