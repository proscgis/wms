package com.pr.wms.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 顾客
 */
public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	// 微信名字
	private String nickname;
	// 地址
	private String address;
	// 用户的标识，对当前公众号唯一
	private String openid;
	// 电话
	private int phone;
	// 用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
	private Timestamp subscribeTime;
	private Timestamp updatetime;
	private String sex;
	private String remark;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public Timestamp getSubscribeTime() {
		return subscribeTime;
	}

	public void setSubscribeTime(Timestamp subscribeTime) {
		this.subscribeTime = subscribeTime;
	}

	public Timestamp getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
