package com.pr.wms.model;

import java.io.Serializable;

/**
 * 分类
 */
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	// 大分类ID(根节点分类id为1)
	private int cateId;
	// 类名
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCateId() {
		return cateId;
	}

	public void setCateId(int cateId) {
		this.cateId = cateId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getsub() {
		return "{\"cate_id\": " + id + "}";
	}
}
