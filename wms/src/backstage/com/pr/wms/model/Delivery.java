package com.pr.wms.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 运费信息
 */
public class Delivery implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	// 运费类型(0-使用下面express字段的默认模板, 1-使用template_id代表的邮费模板, 详见邮费模板相关API)
	private String deliveryType;
	// 邮费模板ID
	private String templateId;
	// 快递ID
	private String expressId;
	// 运费(单位 : 分)
	private String expressPrice;

	public Map toWechatJsonMap() {
		Map json = new HashMap();
		json.put("delivery_type", 0);
		json.put("template_id", 0);
		List<Map> expressList = new ArrayList<Map>();
		expressList.add(getExpress(10000027));
		expressList.add(getExpress(10000028));
		expressList.add(getExpress(10000029));
		json.put("express", expressList);
		return json;
	}

	public Map getExpress(int idVal) {
		Map express = new HashMap();
		express.put("id", idVal);
		express.put("price", 0);
		return express;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getExpressId() {
		return expressId;
	}

	public void setExpressId(String expressId) {
		this.expressId = expressId;
	}

	public String getExpressPrice() {
		return expressPrice;
	}

	public void setExpressPrice(String expressPrice) {
		this.expressPrice = expressPrice;
	}
}
