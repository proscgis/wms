package com.pr.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.pr.wms.model.Category;

@Component("categoryDao")
public class CategoryDao extends BaseDao {

	public List<Category> findAllCategory() {
		return this.readSqlSession.selectList("com.aixuexiao.dao.CategoryDao.selectAllCategory");
	}

	public List<Category> findCategory(int start, int size, Category category) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", start);
		map.put("size", size);
		map.put("category", category);
		return this.readSqlSession.selectList("com.aixuexiao.dao.CategoryDao.selectCategory", map);
	}

	public List<Category> selectCategoryByCateId(int cateId) {
		return this.readSqlSession.selectList("com.aixuexiao.dao.CategoryDao.selectCategoryByCateId", cateId);
	}

	public int countCategoryByCateId(int cateId) {
		return this.readSqlSession.selectOne("com.aixuexiao.dao.CategoryDao.countCategoryByCateId", cateId);
	}

	public int addCategory(Category category) {
		return writerSqlSession.insert("com.aixuexiao.dao.CategoryDao.addCategory", category);
	}

	public int deleteByCateId(int cateId) {
		return writerSqlSession.delete("com.aixuexiao.dao.CategoryDao.deleteByCateId", cateId);
	}

	public Category findCategoryById(int id) {
		return readSqlSession.selectOne("com.aixuexiao.dao.CategoryDao.selectCategoryById", id);
	}
}
