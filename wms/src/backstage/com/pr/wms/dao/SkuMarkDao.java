package com.pr.wms.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.pr.wms.model.SkuMark;

@Component("skuMarkDao")
public class SkuMarkDao extends BaseDao {

	public List<SkuMark> getSkuMarkOfProduct(long productCode) {
		return this.readSqlSession.selectList("com.pr.wms.dao.SkuMarkDao.selectSkuMarkByProductId", productCode);
	}

	public void addSkuMark(SkuMark sku) {
		writerSqlSession.insert("com.pr.wms.dao.SkuMarkDao.addSkuMark", sku);
	}

}
