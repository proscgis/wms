package com.pr.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.pr.wms.model.Product;

@Component("productDao")
public class ProductDao extends BaseDao {

	public Product findProductById(long id) {
		return this.readSqlSession.selectOne("com.pr.wms.dao.ProductDao.selectProductById", id);
	}

	public Product findProductByOpenId(String openid) {
		return this.readSqlSession.selectOne("com.pr.wms.dao.ProductDao.findProductByOpenId", openid);
	}

	public int removeProductOpenId(int id) {
		return this.readSqlSession.update("com.pr.wms.dao.ProductDao.removeProductOpenId", id);
	}

	public List<Product> findProduct(int start, int size, Product product) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", start);
		map.put("size", size);
		map.put("product", product);
		return this.readSqlSession.selectList("com.pr.wms.dao.ProductDao.selectProduct", map);
	}

	public int countProduct(Product product) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("product", product);
		return this.readSqlSession.selectOne("com.pr.wms.dao.ProductDao.selectProductCount", map);
	}

	public List<Product> findProductByCategoryId(int categorysid) {
		return this.readSqlSession.selectList("com.pr.wms.dao.ProductDao.selectProductByCategoryId", categorysid);
	}

	public int addProduct(Product product) {
		return this.writerSqlSession.insert("com.pr.wms.dao.ProductDao.addProduct", product);
	}

	public int deleteProductById(long productid) {
		return this.writerSqlSession.delete("com.pr.wms.dao.ProductDao.deleteProductById", productid);
	}

	public int updateProduct(Product product) {
		return this.writerSqlSession.update("com.pr.wms.dao.ProductDao.updateProduct", product);
	}

	public Integer findMaxId() {
		return this.writerSqlSession.selectOne("com.pr.wms.dao.ProductDao.selectProductMaxId");
	}

}
