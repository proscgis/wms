package com.pr.wms.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.pr.wms.dao.SkuMarkDao;
import com.pr.wms.model.SkuMark;

@Service("skuMarkService")
public class SkuMarkService {

	@Resource(name = "skuMarkDao")
	private SkuMarkDao skuMarkDao;

	/**
	 * 保存规格信息
	 * 
	 * @param sku
	 */
	public void addSkuMark(SkuMark sku) {
		skuMarkDao.addSkuMark(sku);
	}

	public List<SkuMark> getSkuMarkOfProduct(long productCode) {
		return skuMarkDao.getSkuMarkOfProduct(productCode);
	}
}
