package com.pr.wms.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.pr.wms.dao.CategoryDao;
import com.pr.wms.model.Category;

@Service("categoryService")
public class CategoryService {

	@Resource(name = "categoryDao")
	private CategoryDao categoryDao;

	/**
	 * 将数据库中Category数据分页查出
	 * 
	 * @param start
	 *            其实数据条数
	 * @param size
	 *            展示数据每页的大小
	 */
	public List<Category> listCategory(int start, int size, Category category) {
		return categoryDao.findCategory(start, size, category);
	}

	public List<Category> selectCategoryByCateId(int cateId) {
		return categoryDao.selectCategoryByCateId(cateId);
	}

	public int countCategoryByCateId(int cateId) {
		return categoryDao.countCategoryByCateId(cateId);
	}

	/**
	 * 添加分类到数据库中
	 * 
	 * @param category
	 *            分类对象
	 */
	public int addCategory(Category category) {
		return categoryDao.addCategory(category);
	}

	/**
	 * 删除数据库中对应id的分类信息
	 * 
	 * @param categoryId
	 *            分类id
	 */
	public int deleteByCateId(int cateId) {
		return categoryDao.deleteByCateId(cateId);
	}

	/**
	 * 根据id查找对应的Category对象
	 * 
	 * @param id
	 *            分类编号
	 * @return
	 */
	public Category findCategoryById(int id) {
		return categoryDao.findCategoryById(id);
	}

}
