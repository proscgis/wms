package com.pr.wms.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.pr.wms.dao.CategoryDao;
import com.pr.wms.dao.ProductDao;
import com.pr.wms.model.Category;
import com.pr.wms.model.Product;

@Service("productService")
public class ProductService {

	@Resource(name = "productDao")
	private ProductDao productDao;

	@Resource(name = "categoryDao")
	private CategoryDao categoryDao;

	/**
	 * 将数据库中Product数据分页查出
	 * 
	 * @param start
	 *            起始数据条数
	 * @param size
	 *            展示数据每页的大小
	 */
	public List<Product> listProduct(int start, int size, Product product) {
		return productDao.findProduct(start, size, product);
	}

	public int countProduct(Product product) {
		return productDao.countProduct(product);
	}

	public int deleteProductById(long productid) {
		return productDao.deleteProductById(productid);
	}

	public int updateProduct(Product product) {
		return productDao.updateProduct(product);
	}
	public int addProduct(Product product) {
		return productDao.addProduct(product);
	}

	public int findMaxId() {
		Integer maxId = productDao.findMaxId();
		if (maxId != null)
			return maxId;
		return 1;
	}

	/**
	 * 根据商品编号查找对应的商品
	 * 
	 * @param id
	 *            商品编号
	 * @return 商品数据
	 */
	public Product findProductById(long id) {
		return productDao.findProductById(id);
	}

	public Product findProductByOpenId(String openid) {
		if (StringUtils.isEmpty(openid))
			return null;
		return productDao.findProductByOpenId(openid);
	}

	public int removeProductOpenId(int id) {
		return productDao.removeProductOpenId(id);
	}

	/**
	 * 将数据库中Category数据全部查出
	 */
	public List<Category> findAllCategory() {
		return categoryDao.findAllCategory();
	}

}
