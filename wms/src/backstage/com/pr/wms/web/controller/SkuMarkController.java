package com.pr.wms.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pr.wms.model.SkuMark;
import com.pr.wms.service.SkuMarkService;
import com.pr.wms.util.GsonUtils;

/**
 * 规格
 */
@Controller
public class SkuMarkController {
	@Resource(name = "skuMarkService")
	private SkuMarkService skuMarkService;

	/**
	 * 获取某种商品的sku
	 * 
	 * @param productCode
	 * @return
	 */
	@RequestMapping(value = "/{productId}/skus", method = RequestMethod.GET)
	public String list(@PathVariable("productId") long productCode) {
		List<SkuMark> list = skuMarkService.getSkuMarkOfProduct(productCode);
		return GsonUtils.toJson(list);
	}
}
