package com.pr.wms.web.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pr.wms.model.Category;
import com.pr.wms.service.CategoryService;
import com.pr.wms.service.ProductService;
import com.pr.wms.util.GsonUtils;
import com.pr.wms.web.view.MessageView;
import com.pr.wms.wechat.service.WechatCategoryService;
import com.wechat.sdk.util.Token;
import com.wechat.sdk.util.WeChatUtil;

/**
 * 包含分类列表菜单内的所有操作
 */
@Controller
public class CategoryController {
	protected static final Logger log = LoggerFactory.getLogger(CategoryController.class);

	public static int pagesize = 8;

	@Resource(name = "categoryService")
	private CategoryService categoryService;
	@Resource(name = "productService")
	private ProductService productService;
	@Resource(name = "wechatCategoryService")
	private WechatCategoryService wechatCategoryService;

	/**
	 * 调用微信接口获取分类信息
	 * 
	 * @param cateId
	 * @return
	 */
	@RequestMapping(value = "/category/fetchsub/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String fetchCategorySub(@PathVariable("id") int cateId) {
		int num = categoryService.countCategoryByCateId(cateId);
		MessageView msg = new MessageView(MessageView.SUCCESS);
		List<Category> datas = null;
		if (num <= 0) {
			try {
				Token accessToken = WeChatUtil.getToken();
				datas = wechatCategoryService.getCategory(accessToken.getAccessToken(), cateId);
				msg.setData(datas);
			} catch (Exception e) {
				log.error("调用微信接口获取分类信息:", e);
				msg.setStatus(MessageView.FAIL);
				msg.setMsg("调用微信接口获取分类信息:" + e.getMessage());
			}
		}
		return GsonUtils.toJson(msg);
	}

	@RequestMapping(value = "/category/list", method = RequestMethod.POST)
	@ResponseBody
	public String queryCategorySub(@RequestParam String aoData) {
		Map<String, Object> params = GsonUtils.fromJson(aoData);
		int draw = Integer.valueOf((String) params.get("draw"));
		// 第一条数据的起始位置，比如0代表第一条数据
		int start = (Integer) params.get("start");
		// 告诉服务器每页显示的条数，这个数字会等于返回的
		// data集合的记录数，可能会大于因为服务器可能没有那么多数据。这个也可能是-1，代表需要返回全部数据(尽管这个和服务器处理的理念有点违背)
		int length = (Integer) params.get("length");
		String name = (String) params.get("name");
		Integer cateId = (Integer) params.get("cateId");
		Category c = new Category();
		c.setCateId(cateId);
		c.setName(name);
		List<Category> datas = categoryService.listCategory(start, length, c);
		return GsonUtils.toJson(datas);
	}

	/**
	 * 获取子类
	 * 
	 * @param cateId
	 * @return
	 */
	@RequestMapping(value = "/category/getsub/{cateId}", method = RequestMethod.POST)
	@ResponseBody
	public String getCategorySub(@PathVariable("cateId") int cateId) {
		List<Category> datas = categoryService.selectCategoryByCateId(cateId);
		return GsonUtils.toJson(datas);
	}

	/**
	 * 删除子类
	 * 
	 * @param cateId
	 * @return
	 */
	@RequestMapping(value = "/category/delete/{cateId}", method = RequestMethod.GET)
	public String deletecategory(@PathVariable("cateId") int cateId) {
		int status = categoryService.deleteByCateId(cateId);
		String info = "";
		if (status >= 1) {
			info = " 删除成功!";
		}
		MessageView msg = new MessageView(status, info);
		return GsonUtils.toJson(msg);
	}
}
