package com.pr.wms.web.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pr.wms.model.Product;
import com.pr.wms.service.ProductService;
import com.pr.wms.util.GsonUtils;
import com.pr.wms.web.view.Grid;
import com.pr.wms.web.view.MessageView;

/**
 * 包含商品列表菜单内的所有操作
 */
@Controller()
public class ProductController {
	protected static final Logger log = LoggerFactory.getLogger(ProductController.class);

	public static final int pagesize = 8;

	@Resource(name = "productService")
	private ProductService productService;

	/**
	 * 列表
	 *
	 * @return
	 */
	@RequestMapping(value = "/product/list", method = RequestMethod.GET)
	public String list() {
		return "products";
	}

	@RequestMapping(value = "/product/getData", produces = "text/json;charset=UTF-8")
	@ResponseBody
	public String list(@RequestParam String aoData) {
		Map<String, Object> params = null;
		int draw = 0;
		// 第一条数据的起始位置，比如0代表第一条数据
		int start = 0;
		// 告诉服务器每页显示的条数，这个数字会等于返回的
		// data集合的记录数，可能会大于因为服务器可能没有那么多数据。这个也可能是-1，代表需要返回全部数据(尽管这个和服务器处理的理念有点违背)
		int length = 10;
		if (aoData != null) {
			params = GsonUtils.fromJson(aoData);
			draw = ((Double) params.get("start")).intValue();
			start = ((Double) params.get("start")).intValue();
			length = ((Double) params.get("length")).intValue();
		}
		Product product = new Product();
		if (params != null && params.get("name") != null) {
			product.setName((String) params.get("name"));
		}
		List<Product> list = productService.listProduct(start, length, product);
		int count = productService.countProduct(product);
		Grid<Product> grid = new Grid<Product>();
		grid.setData(list);
		grid.setRecordsTotal(count);
		grid.setDraw(draw);
		return GsonUtils.toJson(grid);
	}

	@RequestMapping(value = "/product/add", method = RequestMethod.POST, produces = "text/json;charset=UTF-8")
	@ResponseBody
	public String add(Product product) {
		// @RequestParam("mainImgFile")MultipartFile file,
		int status = productService.addProduct(product);
		String info = "";
		if (status == 1) {
			info = product.getName() + " 添加成功!";
		}
		MessageView msg = new MessageView(status, info);
		return GsonUtils.toJson(msg);
	}

	@RequestMapping(value = "/product/get/{id}", method = RequestMethod.GET, produces = "text/json;charset=UTF-8")
	@ResponseBody
	public String get(@PathVariable("id") long id) {
		Product product;
		if (id == 0) {
			product = new Product();
		} else {
			product = productService.findProductById(id);
		}
		MessageView msg = new MessageView(MessageView.SUCCESS);
		msg.setData(product);
		return GsonUtils.toJson(msg);
	}

	@RequestMapping(value = "/product/update", method = RequestMethod.POST, produces = "text/json;charset=UTF-8")
	@ResponseBody
	public String update(Product product) {
		int status = productService.updateProduct(product);
		String info = "";
		if (status == 1) {
			info = product.getName() + " 修改成功!";
		}
		MessageView msg = new MessageView(status, info);
		return GsonUtils.toJson(msg);
	}

	@RequestMapping(value = "/product/del/{id}", method = RequestMethod.DELETE, produces = "text/json;charset=UTF-8")
	@ResponseBody
	public String delete(@PathVariable("id") long id) {
		int status = productService.deleteProductById(id);
		MessageView msg = new MessageView(status);
		return GsonUtils.toJson(msg);
	}

}
