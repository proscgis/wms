package com.pr.wms.web.view;

public class MessageView {
	public static final int SUCCESS = 1;
	public static final int FAIL = 0;
	private String id;

	private int status;

	private String msg;
	private Object data;

	public MessageView() {

	}

	public MessageView(int status) {
		super();
		this.status = status;
	}

	public MessageView(int status, String msg) {
		super();
		this.status = status;
		this.msg = msg;
	}

	public MessageView(String id, String msg) {
		super();
		this.id = id;
		this.msg = msg;
	}

	public MessageView(String id, int status) {
		super();
		this.id = id;
		this.status = status;
	}

	public MessageView(String id, int status, String msg) {
		super();
		this.id = id;
		this.status = status;
		this.msg = msg;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}