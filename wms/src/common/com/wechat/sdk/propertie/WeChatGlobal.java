/*
 * 文件名：WeixinGlobal.java
 * 版权：Copyright by 791440142
 * 描述：
 * 修改人：yu_qhai
 * 修改时间：2016年3月20日
 */

package com.wechat.sdk.propertie;

/**
 * 微信全局变量配置
 *
 */
public class WeChatGlobal {
	public static final String DOMAIN = "http://asdfghj.ngrok.cc/wms";
	public static final String APPID = "wxa5649e040907b5c2";
	public static final String APPSECRET = "e442b3d416e70e9baf0389eac777158c";
	public static final String TOKEN = "wms";
	// 获取全局Access Token请求模版
	public static final String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
	// 获取openid的请求模版
	public static final String ACCESS_OPENID = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=APPSECRET&code=CODE&grant_type=authorization_code";
	public static final String ACCESS_USER = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID";
	// 创建个性化菜单
	public static final String MENU_ADDCONDITIONAL = "https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=ACCESS_TOKEN";
	// 上传图文消息内的图片获取URL
	public static final String MEDIA_UPLOADIMG = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN";

	/**
	 * 增加商品
	 */
	public static final String MERCHANT_PRODUCT_CREATE = "https://api.weixin.qq.com/merchant/create?access_token=ACCESS_TOKEN";
	/**
	 * 1.7 获取指定分类的所有子分类
	 */
	public static final String MERCHANT_CATEGORY_GETSUB = "https://api.weixin.qq.com/merchant/category/getsub?access_token=ACCESS_TOKEN";
	/**
	 * 1.8 获取指定子分类的所有SKU
	 */
	public static final String MERCHANT_CATEGORY_GETSKU = "https://api.weixin.qq.com/merchant/category/getsku?access_token=ACCESS_TOKEN";
	// 5. 货架管理接口
	/**
	 * 增加货架
	 */
	public static final String MERCHANT_SHELF_ADD = "https://api.weixin.qq.com/merchant/shelf/add?access_token=ACCESS_TOKEN";
	/**
	 * 增加分组
	 */
	public static final String MERCHANT_GROUP_ADD = "https://api.weixin.qq.com/merchant/group/add?access_token=ACCESS_TOKEN";

	private WeChatGlobal() {
	}
}
