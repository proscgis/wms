package com.pr.wms.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.pr.wms.model.Product;
import com.pr.wms.web.view.Grid;

public class GsonUtils {
	private GsonUtils() {
	}

	public static String toJson(Map map) {
		return new Gson().toJson(map);
	}

	public static String toJson(Object obj) {
		return new Gson().toJson(obj);
	}

	public static Map fromJson(String json) {
		return new Gson().fromJson(json, Map.class);
	}

	public static <T> T fromJson(String json, Class<T> calzz) {
		return new Gson().fromJson(json, calzz);
	}

	public static void main(String[] args) throws Exception {
		String s = "http%3A%2F%2Fvote1.qingdaonews.com%2Fnews%2F201612%2Fintelligence%2FwapShowOneProgram.php%3Fact%3DMxTuUlyi&response_type=code&scope=snsapi_base&state=123&connect_redirect=1#wechat_redirect&openid=123";
		System.out.println(URLDecoder.decode(s));
	}

	public static void test1() {
		Grid<Product> grid = new Grid<Product>();
		List<Product> data = new ArrayList<Product>();
		Product e = new Product();
		e.setName("hello");
		data.add(e);
		grid.setData(data);
		System.out.println(toJson(grid));
	}
}
