package com.pr.wms.util;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class CreateTable {
	private static String[] tables = { "Category", "Customer", "Delivery", "Goods", "Group", "Order", "Product", "Property", "Shelf", "Sku", "SkuMark" };

	public static void main(String[] args) throws Exception {
		StringBuilder sb = new StringBuilder();
		for (String table : tables) {
			Class clazz = CreateTable.class.getClassLoader().loadClass("com.pr.wms.model." + table);
			sb.append("\n-- drop table t_" + Underline2Camel.camel2Underline(clazz.getSimpleName()) + " ;");
			sb.append("\ncreate table t_" + Underline2Camel.camel2Underline(clazz.getSimpleName()) + "(\n");
			for (Field f : clazz.getDeclaredFields()) {
				if (!"serialVersionUID".equals(f.getName())) {
					try {
						Object dbType = getDbType(f.getType());
						sb.append(Underline2Camel.camel2Underline(f.getName())).append("\t").append(dbType);
						sb.append(",\n");
					} catch (Exception e) {
						System.out.println(clazz.getSimpleName() + "\t" + f.getName() + "\t" + f.getType());
					}
				}
			}
			sb.append("primary key (id)");
			sb.append(");");
		}
		File sql = new File("D:\\test\\wechat\\tables.sql");
		FileWriter w = new FileWriter(sql);
		w.write(sb.toString());
		w.close();
	}

	private static Object getDbType(Class<?> type) throws Exception {
		if (type.equals(String.class)) {
			return "varchar(40)";
		}
		if (type.equals(Timestamp.class)) {
			return "datetime";
		}
		if (type.equals(long.class)) {
			return "bigint(20)";
		}
		if (type.equals(int.class)) {
			return "int";
		}
		if (type.equals(BigDecimal.class)) {
			return "DECIMAL";
		}
		throw new Exception("没有找到类型");
	}
}
