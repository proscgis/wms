package com.pr.wms.util;

public enum UploadState {
	UPLOAD_SUCCSSS(0, "成功"), UPLOAD_FAILURE(1, "失败"), UPLOAD_TYPE_ERROR(2, "出错"), UPLOAD_OVERSIZE(3, "超过大小"), UPLOAD_ZEROSIZE(4, "空文件"), UPLOAD_NOTFOUND(5,
			"没有找到文件");

	private String state;
	private int flag;

	public String getState() {
		return this.state;
	}

	public int getFlag() {
		return this.flag;
	}

	UploadState(int flag, String state) {
		this.state = state;
		this.flag = flag;
	}
}