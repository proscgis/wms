
-- drop table t_CATEGORY ;
create table t_CATEGORY(
ID	int,
CATE_ID	int,
NAME	varchar(40),
primary key (id));
-- drop table t_CUSTOMER ;
create table t_CUSTOMER(
ID	int,
NICKNAME	varchar(40),
ADDRESS	varchar(40),
OPENID	varchar(40),
PHONE	int,
SUBSCRIBE_TIME	datetime,
UPDATETIME	datetime,
SEX	varchar(40),
REMARK	varchar(40),
primary key (id));
-- drop table t_DELIVERY ;
create table t_DELIVERY(
ID	int,
DELIVERY_TYPE	varchar(40),
TEMPLATE_ID	varchar(40),
EXPRESS_ID	varchar(40),
EXPRESS_PRICE	varchar(40),
primary key (id));
-- drop table t_GOODS ;
create table t_GOODS(
ID	int,
MERCHANDISE_ID	varchar(40),
QUANTITY	int,
REMARK	varchar(40),
primary key (id));
-- drop table t_GROUP ;
create table t_GROUP(
ID	int,
NAME	varchar(40),
primary key (id));
-- drop table t_ORDER ;
create table t_ORDER(
ID	varchar(40),
ADDRESS	varchar(40),
DELIVERY_TIME	datetime,
CREATETIME	datetime,
primary key (id));
-- drop table t_PRODUCT ;
create table t_PRODUCT(
ID	bigint(20),
NAME	varchar(40),
CATEGORY_ID	int,
CATEGORY_NAME	varchar(40),
MAIN_IMG	varchar(40),
IMG	varchar(40),
DETAIL_TEXT	varchar(40),
DETAIL_IMG	varchar(40),
BUY_LIMIT	int,
CREATE_TIME	datetime,
UPDATE_TIME	datetime,
primary key (id));
-- drop table t_PROPERTY ;
create table t_PROPERTY(
ID	varchar(40),
VID	varchar(40),
primary key (id));
-- drop table t_SHELF ;
create table t_SHELF(
ID	int,
EID	int,
primary key (id));
-- drop table t_SKU ;
create table t_SKU(
ID	int,
CATE_ID	int,
NAME	varchar(40),
ORI_PRICE	int,
PRICE	int,
ICON_URL	varchar(40),
QUANTITY	int,
PRODUCT_CODE	varchar(40),
primary key (id));
-- drop table t_SKU_MARK ;
create table t_SKU_MARK(
ID	int,
CATE_ID	int,
NAME	varchar(40),
ORI_PRICE	int,
PRICE	int,
ICON_URL	varchar(40),
QUANTITY	int,
PRODUCT_CODE	varchar(40),
primary key (id));