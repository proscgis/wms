<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>员工积分记录</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@include file="/WEB-INF/views/common/css.jsp" %>
	</head>

	<body>
		<%@ include file="/WEB-INF/views/common/navbar.jsp" %>
		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
			<%@ include file="/WEB-INF/views/common/sidebar.jsp" %>
			<div class="main-content">
				 
				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							<small>
								<i class="icon-comment"></i>
								员工积分记录
							</small>
						</h1>
					</div> 
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							 <!--PAGE CONTENT BEGINS-->
							<form class="form-inline" method="post" id="searchScoreNote" action="<%=request.getContextPath() %>/manager/scoreNotes">
								<input type="hidden" name="pagenum" id="pagenum" value="${pagenum}"/>
								&nbsp;&nbsp;员工编号：
								<input type="text" name="userid" value="${scoreNote.userid==0?'':scoreNote.userid }"  class="input-small search-query"/>&nbsp;&nbsp;
								&nbsp;&nbsp;员工姓名：
								<input type="text" name="sname" value="${scoreNote.sname}"  class="input-small search-query"/>&nbsp;&nbsp;
								<select name="classid" class="input-medium">
									<option value="0">选择公司</option>
									<c:forEach items="${clsList}"  var="cls"  >
										<option <c:if test="${scoreNote.classid == cls.id}">selected="selected"</c:if> value="${cls.id}">${cls.name}</option>
									</c:forEach>
								</select>&nbsp;&nbsp;
								<button  type="submit" class="btn btn-purple btn-small">
									查找
									<i class="icon-search icon-on-right bigger-110"></i>
								</button>
								<button  type="button" class="btn btn-purple btn-small" onclick="location.href='<%=request.getContextPath() %>/manager/scoreNotes'" >
									清空
									<i class="icon-remove icon-on-right bigger-110"></i>
								</button>
							</form>
							
							<c:if test="${param.notice != null}">
								<div class="alert alert-info">
									<button type="button" class="close" data-dismiss="alert">
										<i class="icon-remove"></i>
									</button>
									<i class="icon-ok"></i>
									<strong>${param.notice}</strong>
								</div>
							</c:if>
							<hr>
							<table id="sample-table-1" class="table table-striped table-bordered table-hover">
								<thead>
									<tr >
								      <th width="5%">签到编号</th>
								      <th width="10%">员工姓名</th>
								      <th width="10%">所在公司</th>
								      <th width="10%">签到积分</th>
								      <th width="15%">签到日期</th>
								    </tr>
								  </thead>
								  <tbody>
								  	<c:forEach items="${grid.rows}"  var="message"   varStatus="st" >
								  		<tr>
								  		<td>${message.id}</td>
								  		<td>${message.sname}</td>
								  		<td>${message.cname}</td>
								  		<td>${message.score}</td>
								  		<td><fmt:formatDate value="${message.day}" pattern="yyyy-MM-dd"/></td>
								  		</tr>
								  	</c:forEach>
								  </tbody>
							</table>
							
					 		<div class="dataTables_paginate paging_bootstrap pagination">
							  <c:if test="${grid.pages > 1}">
							  <button class="btn btn-success btn-mini" onclick="searchScoreNote(1)">首页</button>
							  <select name="classid" id="editclassid" class="btn btn-success input-medium" onchange="searchScoreNote(this.value)">
							  	<c:forEach  varStatus="num" begin="1" end="${grid.pages }">
									<option value="${num.index }" <c:if test="${num.index ==grid.currentPage }"> selected="selected"</c:if>>第${num.index }页</option>
								</c:forEach>
							  </select>
							  <button class="btn btn-success btn-mini" onclick="searchScoreNote(${grid.pages })" >末页</button>
							 </c:if>
					 		</div>
							<!--PAGE CONTENT ENDS-->
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div><!--/.page-content-->
		</div><!--/.main-content-->
	</div><!--/.main-container-->

		<%@include file="/WEB-INF/views/common/js.jsp" %>
		<script type="text/javascript">
			function searchScoreNote(pagenum){
				$("#pagenum").val(pagenum);
				$("#searchScoreNote").submit();
			}
		</script>
	</body>
</html>