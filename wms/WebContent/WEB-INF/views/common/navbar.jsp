<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="navbar">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a href="<%=request.getContextPath()%>/" class="brand">
				<small>
					<i class="icon-briefcase"></i>
					&nbsp;后台系统 
				</small>
			</a><!--/.brand-->
			<div style="float:right;">
				<a href="#updateAdminPass" class="brand" data-toggle="modal">
					<small>
						<i class="icon-wrench"></i>
						&nbsp;修改密码
					</small>
				</a>
				<a href="<%=request.getContextPath()%>/loginout" class="brand">
					<small>
						<i class="icon-off"></i>
						&nbsp;退出 
					</small>
				</a>
			</div>
		</div><!--/.container-fluid-->
	</div><!--/.navbar-inner-->
</div>
<div id="updateAdminPass" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<form  class="form-inline" id="updateAdministrator" method="post" action="updateAdministrator" class="form-inline">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		    <h3 id="myModalLabel">修改密码</h3>
		  </div>
		  <div class="modal-body">
			<label class="control-label"  >　　帐号:&nbsp;&nbsp;</label>
			<input type="text"  name="username" value="${user }" readonly="readonly" /><br/>
			<label class="control-label"   >　原密码:&nbsp;&nbsp;</label>
			<input type="text" name="password" id="password"  placeholder="请输入原密码"/><br/>
			<label class="control-label"   >　新密码:&nbsp;&nbsp;</label>
			<input type="text" name="newpassword" id="newpassword"    placeholder="请输入新密码"/><br/>
			<label class="control-label"   >确认密码:&nbsp;&nbsp;</label>
			<input type="text" name="repassword" id="repassword"    placeholder="请输入确认密码"/>
		  </div>
		  <div class="modal-footer">
		    <button  type="button" id="modifyadmin" onclick="checkPassword()" class="btn btn-small btn-primary">修改</button>
		  </div>
	</form>
</div>
<%
String passwdmsg = request.getParameter("passwdmsg");
if(passwdmsg!=null&&passwdmsg.trim()!=""){
%>
<script type="text/javascript">
<!--
alert('<%=passwdmsg %>');
//-->
</script>
<%	
}
%>
<script type="text/javascript">
//管理员密码修改 
function checkPassword(){
	if($.trim($("#password").val())==''){
		alert('请输入原密码！');
		return;
	}if($.trim($("#newpassword").val())==''){
		alert('请输入新密码！');
		return;
	}if($("#repassword").val()!=$("#newpassword").val()){
		alert('新密码与确认密码不匹配！');
		return;
	}else{
		$("#updateAdministrator").submit();
	}
}
</script>

