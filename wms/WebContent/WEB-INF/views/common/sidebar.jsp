<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="sidebar responsive" id="sidebar" data-sidebar="true"
	data-sidebar-scroll="true" data-sidebar-hover="true">
	<script type="text/javascript">
		try {
			ace.settings.loadState('sidebar')
		} catch (e) {
		}
	</script>
	<ul class="nav nav-list">
		<li <c:if test="${sidebar=='products'}">class="active"</c:if>><a
			href="<%=request.getContextPath()%>/manager/products"> <i
				class="icon-user"></i> <span class="menu-text"> 商品管理 </span>
		</a></li>
		<li <c:if test="${sidebar=='classes'}">class="active"</c:if>><a
			href="<%=request.getContextPath()%>/manager/classes"> <i
				class="icon-list-alt"></i> <span class="menu-text"> 库存管理 </span>
		</a></li>
		<li <c:if test="${sidebar=='club'}">class="active"</c:if>><a
			href="<%=request.getContextPath()%>/manager/club"> <i
				class="icon-list-alt"></i> <span class="menu-text"> 订单管理 </span>
		</a></li>
		<li <c:if test="${sidebar=='active'}">class="active"</c:if>><a
			href="<%=request.getContextPath()%>/manager/active"> <i
				class="icon-list-alt"></i> <span class="menu-text"> 分组管理 </span>
		</a></li>
		<li
			<c:if test="${sidebar=='productmessages'||sidebar=='messages'||sidebar=='replys'}">class="active"</c:if>>
			<a href="#" class="dropdown-toggle"> <i class="icon-comment"></i>
				<span class="menu-text"> 微信消息 </span> <b
				class="arrow icon-angle-down"></b>
		</a>
			<ul class="submenu">
				<%-- <li <c:if test="${sidebar=='messages'}">class="active"</c:if> >
					<a href="<%=request.getContextPath()%>/manager/messages">
						<i class="icon-double-angle-right"></i>
						接受消息
					</a>
				</li> 
				<li <c:if test="${sidebar=='replys'}">class="active"</c:if> >
					<a href="<%=request.getContextPath()%>/manager/replys">
						<i class="icon-double-angle-right"></i>
						推送消息
					</a>
				</li>
				--%>
				<li <c:if test="${sidebar=='productmessages'}">class="active"</c:if>>
					<a href="<%=request.getContextPath()%>/manager/productmessages">
						<i class="icon-double-angle-right"></i> 用户反馈
				</a>
				</li>

			</ul>
		</li>
		<li
			<c:if test="${sidebar=='scoremessages'||sidebar=='score'}">class="active"</c:if>>
			<a href="#" class="dropdown-toggle"> <i class="icon-list-alt"></i>
				<span class="menu-text"> 积分签到 </span> <b
				class="arrow icon-angle-down"></b>
		</a>
			<ul class="submenu">
				<li <c:if test="${sidebar=='scoremessages'}">class="active"</c:if>>
					<a href="<%=request.getContextPath()%>/manager/prizeActive"> <i
						class="icon-double-angle-right"></i> 活动管理
				</a>
				</li>
				<li <c:if test="${sidebar=='scoreNotes'}">class="active"</c:if>>
					<a href="<%=request.getContextPath()%>/manager/scoreNotes"> <i
						class="icon-double-angle-right"></i> 签到记录
				</a>
				</li>
				<li <c:if test="${sidebar=='prizeNotes'}">class="active"</c:if>>
					<a href="<%=request.getContextPath()%>/manager/prizeNotes"> <i
						class="icon-double-angle-right"></i> 中奖记录
				</a>
				</li>
			</ul>
		</li>

	</ul>
	<!--/.nav-list-->
	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i class="icon-double-angle-left" id="sidebar-toggle-icon"
			data-icon2="icon-double-angle-right"
			data-icon1="icon-double-angle-left"></i>
	</div>
</div>