<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>商品信息</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<%@include file="/WEB-INF/views/common/css.jsp"%>
<link href="<%=request.getContextPath()%>/assets/js/datatables/dataTables.bootstrap.css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/assets/js/datatables/jquery.dataTables.css" rel="stylesheet" />
<style type="text/css" ></style>
</head>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
var webPath = "<%=path %>"
</script>
<body>
	<%@ include file="/WEB-INF/views/common/navbar.jsp"%>
	<div class="main-container ace-save-state" id="main-container">
		<a class="menu-toggler" id="menu-toggler" href="#"> <span
			class="menu-text"></span>
		</a>
		<%@ include file="/WEB-INF/views/common/sidebar.jsp"%>
		<div class="main-content">
			<div class="main-content-inner">

				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							<small> <i class="icon-user"></i> 商品信息
							</small>
						</h1>
					</div>
					<div class="row-fluid">
						<div class="col-xs-12">
							<div class="row-fluid">
								<div class="col-xs-12">
									<form id="queryForm" action="<%=path%>/product/getData"
										method="post">
										<div class="row-fluid">
											<div class="col-xs-12 col-sm-8">
												<div class="input-group">
													<input type="text" class="form-control search-query" id="name" name="name" placeholder="请输入商品名称" />
													<span class="input-group-btn">
														<button type="button" class="btn btn-purple btn-sm">
															查询
															<i class="icon-search icon-on-right bigger-110"></i>
														</button>
													</span>
												</div>
											</div>
										</div>
									</form>
									<div class="pull-right">
										<div class="btn-group">
											<button type="button" class="btn btn-primary btn-sm"
												id="btn-add">
												<i class="fa fa-plus"></i> 增加
											</button>
											<button type="button" class="btn btn-primary btn-sm"
												id="btn-delAll">
												<i class="fa fa-remove"></i>删除
											</button>
											<button type="button" class="btn btn-primary btn-sm"
												id="btn-re">
												<i class="fa fa-refresh"></i>刷新
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class="row-fluid">
								<div class="col-xs-12">
									<div class="dataTables_wrapper form-inline no-footer">
										<table id="dataTable"
											class="table table-striped table-bordered table-hover dataTable no-footer"
											align="center">
											<thead>
												<tr class="info">
													<td><input type="checkbox" id="checkAll"></td>
													<th>ID</th>
													<th>名称</th>
													<th>分类</th>
													<th>限购</th>
													<th>添加时间</th>
													<th>修改时间</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--/.page-content-->
				</div>
				<!--/.main-content-->
			</div>
		</div>
		<!--/.main-container-->
		<%@include file="/WEB-INF/views/common/js.jsp"%>
		
		<!-- Edit -->
		<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog" >
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">商品信息</h4>
					</div>
					<div class="modal-body" >
						<form class="form-horizontal"  id="editForm" method="post" ectype="multipart/form-data"> 
							<input type="hidden" class="form-control" name="id" value="0">
							<div class="form-group">
								<label for="name" class="col-sm-3 control-label">名称</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="name" id="name">
								</div>
							</div>
							<div class="form-group">
								<label for="categoryId" class="col-sm-3 control-label">分类</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="categoryId" id="categoryId">
								</div>
							</div>
							<div class="form-group">
								<label for="buyLimit" class="col-sm-3 control-label">限购</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="buyLimit" id="buyLimit">
								</div>
							</div>
							<div class="form-group">
								<label for="mainImg" class="col-sm-3 control-label">主图</label>
								<div class="col-sm-9">
								<input type="file" id="mainImgFile" name="mainImgFile"/>
			                    </div>
							</div>
						</form>
						<div class="alert alert-warning" id="showMessageDiv" style="display: none;">
							<strong>保存失败!</strong>
							<span id="showMessage"></span>
							<br />
						</div>
					</div>
					<!-- modal-body END -->
					<div class="modal-footer">
						<button id="btn-submit" type="button" class="btn btn-primary" >提交</button>
					</div>
				</div>
			</div>
		</div>
		<script
			src="<%=request.getContextPath()%>/assets/js/datatables/jquery.dataTables.min.js"></script>
		<script
			src="<%=request.getContextPath()%>/assets/js/datatables/dataTables.bootstrap.js"></script>
		<script src="<%=request.getContextPath()%>/assets/js/dropzone.min.js"></script>
		<script src="<%=request.getContextPath()%>/assets/js/bootbox.min.js"></script>
		<script src="<%=request.getContextPath()%>/assets/js/jquery.form.js"></script>
		<script src="<%=request.getContextPath()%>/assets/js/wms/products.js"></script>
</body>
</html>