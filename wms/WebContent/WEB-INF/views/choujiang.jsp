<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
		<title>抽奖</title>
		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<!--basic styles-->
		<link href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<%=request.getContextPath()%>/assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-fonts.css" />
		<!--ace styles-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-skins.min.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-ie.min.css" />
		<![endif]-->
		<style type="text/css">
			span
            {
                line-height:2.5;
            }
		</style>
	</head>
	<body class="login-layout" style="background-color: #cd3031;">
			<div class="main-container container-fluid" >
				<div class="main-content">
							<div class="login-container" >
								<div class="row-fluid">
									<div class="position-relative">
										<div id="login-box" class="login-box visible widget-box no-border">
											<div class="widget-body">
												<div class="widget-main" style="padding-bottom:0px;">
													<div class="center" style="background-color: #ff4f64;">
														<div style="padding-top: 42px;padding-left:4px;width: 300px;height: 300px;margin:auto;background:url(assets/images/btn_choujiang2.png) no-repeat 5px 5px">
															<table class="choujiangtbl" id="choujiangtbl" style="margin: 0 auto;">
															<tr>
															<c:if test="${prizeList[0].num>0 }">
															<td class="lottery-unit lottery-unit-0" id="${prizeList[0].id }"><img src="${prizeList[0].img }"/></td>
															</c:if>
															<c:if test="${prizeList[0].num<=0 }">
															<td class="lottery-unit lottery-unit-0"><img src="assets/images/btn_choujiang_0.png"/></td>
															</c:if>
															<td class="lottery-unit lottery-unit-1"><img src="assets/images/btn_choujiang_0.png"/></td>
															<c:if test="${prizeList[1].num>0 }">
															<td class="lottery-unit lottery-unit-2" id="${prizeList[1].id }"><img src="${prizeList[1].img }"/></td>
															</c:if>
															<c:if test="${prizeList[1].num<=0 }">
															<td class="lottery-unit lottery-unit-2"><img src="assets/images/btn_choujiang_0.png"/></td>
															</c:if>
															</tr>
															<tr>
															<td class="lottery-unit lottery-unit-7"><img src="assets/images/btn_choujiang_0.png"/></td>
															<td><img id="startImg" src="assets/images/btn_choujiang_start.png"/></td>
															<td class="lottery-unit lottery-unit-3"><img src="assets/images/btn_choujiang_0.png"/></td>
															</tr>
															<tr>
															<c:if test="${prizeList[2].num>0 }">
															<td class="lottery-unit lottery-unit-6" id="${prizeList[2].id }"><img src="${prizeList[2].img }"/></td>
															</c:if>
															<c:if test="${prizeList[2].num<=0 }">
															<td class="lottery-unit lottery-unit-6"><img src="assets/images/btn_choujiang_0.png"/></td>
															</c:if>
															<td class="lottery-unit lottery-unit-5"><img src="assets/images/btn_choujiang_0.png"/></td>
															<c:if test="${prizeList[3].num>0 }">
															<td class="lottery-unit lottery-unit-4" id="${prizeList[3].id }"><img src="${prizeList[3].img }"/></td>
															</c:if>
															<c:if test="${prizeList[3].num<=0 }">
															<td class="lottery-unit lottery-unit-4"><img src="assets/images/btn_choujiang_0.png"/></td>
															</c:if>
															</tr>
															</table>
														</div>
														<div class="choujiangmidbg"></div>
													</div>
													<div class="row-fluid choujiangdes" style="background-color: #cd3031;">
													<p style="margin-left:6px;"><img alt="" src="assets/images/btn_choujiang_hddes.png"></p>
													<p style="margin:0px 0px 0px 10px;">亲,请点击"开始按钮"抽奖,祝您好运哦!</p>
													<p style="margin:0px 0px 0px 10px;">活动时间:<fmt:formatDate  value="${prizeActive.starttime}" type="both" pattern="yyyy.MM.dd HH:mm" />至<fmt:formatDate  value="${prizeActive.endtime}" type="both" pattern="yyyy.MM.dd HH:mm" />总共可抽奖一次。</p>
													<p style="margin-left:6px;"><img alt="" src="assets/images/btn_choujiang_hdjp.png"></p>
													<p style="margin:0px 0px 0px 10px;">一等奖：${prizeList[0].name}</p>
													<p style="margin:0px 0px 0px 10px;">二等奖：${prizeList[1].name}</p>
													<p style="margin:0px 0px 0px 10px;">三等奖：${prizeList[2].name}</p>
													<p style="margin:0px 0px 0px 10px;">四等奖：${prizeList[3].name}</p>
													</div>
													</div>
												</div><!--/widget-main-->
											</div><!--/widget-body-->
										</div><!--/login-box-->
	
									</div><!--/position-relative-->
								</div>
								
							<!-- 中奖提示 -->
							<div id="zjModal" class="modal hide fade" tabindex="-1" style="-webkit-border-radius: 40px;border-radius: 40px;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  	<img alt="" src="assets/images/btn_choujiang_zj.png">
							</div>
							<!-- 未中奖提示 -->
							<div id="wzjModal" class="modal hide fade" tabindex="-1" style="-webkit-border-radius: 40px;border-radius: 40px;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  	<img alt="" src="assets/images/btn_choujiang_wzj.png">
							</div>
							<!-- 已抽奖提示 -->
							<div id="ycjModal" class="modal hide fade" tabindex="-1" style="-webkit-border-radius: 40px;border-radius: 40px;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  	<img alt="" src="assets/images/btn_choujiang_ycj.png">
							</div>
							<!-- 奖品抽空提示 -->
							<div id="jpckModal" class="modal hide fade" tabindex="-1" style="-webkit-border-radius: 40px;border-radius: 40px;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  	<img alt="" src="assets/images/btn_choujiang_jcyk.png">
							</div>
							</div>
				</div>
			<!--basic scripts-->
	
			<!--[if !IE]>-->
	
			<script type="text/javascript">
				window.jQuery || document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
			</script>
			<!--<![endif]-->
	
			<!--[if IE]>
			<script type="text/javascript">
			 window.jQuery || document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
			</script>
			<![endif]-->
	
			<script type="text/javascript">
				if("ontouchend" in document) document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
			</script>
			<script src="<%=request.getContextPath()%>/assets/js/bootstrap.min.js"></script>
	
			<!--page specific plugin scripts-->
	
			<!--ace scripts-->
	
			<script src="<%=request.getContextPath()%>/assets/js/ace-elements.min.js"></script>
			<script src="<%=request.getContextPath()%>/assets/js/ace.min.js"></script>
			<script type="text/javascript">
			var click=false;
			var hadPrized = ${hasPrized};
			var lottery={
				index:-1,	//当前转动到哪个位置，起点位置
				count:0,	//总共有多少个位置
				timer:0,	//setTimeout的ID，用clearTimeout清除
				speed:20,	//初始转动速度
				times:0,	//转动次数
				cycle:50,	//转动基本次数：即至少需要转动多少次再进入抽奖环节
				prize:-1,	//中奖位置
				init:function(id){
					if ($("#"+id).find(".lottery-unit").length>0) {
						$lottery = $("#"+id);
						$units = $lottery.find(".lottery-unit");
						this.obj = $lottery;
						this.count = $units.length;
						$lottery.find(".lottery-unit-"+this.index).addClass("active");
					};
				},
				roll:function(){
					//是否已抽奖
					if(true==hadPrized){//已抽过奖
						$('#ycjModal').modal();
						if(lottery){
							clearTimeout(lottery.timer);
							lottery.prize=-1;
							lottery.times=0;
							click=false;
						}
						return;
					}
					var index = this.index;
					var count = this.count;
					var lottery = this.obj;
					$(lottery).find(".lottery-unit-"+index).removeClass("active");
					index += 1;
					if (index>count-1) {
						index = 0;
					};
					$(lottery).find(".lottery-unit-"+index).addClass("active");
					this.index=index;
					return false;
				},
				stop:function(index){
					this.prize=index;
					return false;
				}
			};	
			function roll(){
				lottery.times += 1;
				lottery.roll();
				if (lottery.times > lottery.cycle+10 && lottery.prize==lottery.index) {
					var prizeid = $("#choujiangtbl").find(".lottery-unit-"+lottery.prize).attr("id");
					if(isNaN(parseInt(prizeid))){
						prizeid = 0;
					}
					clearTimeout(lottery.timer);
					lottery.prize=-1;
					lottery.times=0;
					click=false;
					
					$.ajax({
				       url:'<%=request.getContextPath()%>/choujiang', //后台处理程序
				       type:'post',         //数据发送方式
				       dataType:'json',     //接受数据格式
				       data:{userid:'${student.id}',prizeid:prizeid,paid:'${prizeActive.id}'},         //要传递的数据
				       success:function(data){//回传函数(这里是函数名)
				    	   if(data.hasPrized){
					    	 	//获取抽奖位置
								if(lottery.prize %2!=0){//未中奖
									$('#wzjModal').modal();
								}else{//中奖
									$('#zjModal').modal();
								}
				    	   }else{
				    		   alert(data.message);
				    	   }
				    	   hadPrized = data.hasPrized;
				       } 
				    });
				}else{
					if (lottery.times<lottery.cycle) {
						lottery.speed -= 10;
					}else if(lottery.times==lottery.cycle) {
						var index = Math.random()*(lottery.count)|0;
						lottery.prize = index;		
					}else{
						if (lottery.times > lottery.cycle+10 && ((lottery.prize==0 && lottery.index==7) || lottery.prize==lottery.index+1)) {
							lottery.speed += 110;
						}else{
							lottery.speed += 20;
						}
					}
					if (lottery.speed<40) {
						lottery.speed=40;
					};
					lottery.timer = setTimeout(roll,lottery.speed);
				}
				return false;
			}
			$(function(){
				lottery.init('choujiangtbl');
				$("#startImg").click(function(){
					var jpnum = parseInt('${jpsl}');
					if(jpnum<=0){
						$('#jpckModal').modal();
						return;
					}
					if (click) {
						return false;
					}else{
						lottery.speed=100;
						roll();
						click=true;
						return false;
					}
				});
			});
			</script>
		</body>
</html>