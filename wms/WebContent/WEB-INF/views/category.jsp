<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>分类信息</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@include file="/WEB-INF/views/common/css.jsp" %>
	</head>
	<body>
		<%@ include file="/WEB-INF/views/common/navbar.jsp" %>
		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
			<%@ include file="/WEB-INF/views/common/sidebar.jsp" %>
			<div class="main-content">
				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							<small>
								<i class="icon-list-alt"></i>
								分类信息
							</small>
						</h1>
					</div> 
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<form class="form-inline" id="searchClassForm" method="post"  action="<%=request.getContextPath() %>/manager/classes">
								<input type="hidden" id="pagenum" name="pagenum" value="${pagenum}">
								&nbsp;&nbsp;名称：<input type="text" id="s_name" name="name" value="${classes.name}"  class="input-medium search-query">&nbsp;&nbsp;&nbsp;&nbsp;
								<button  type="submit" class="btn btn-purple btn-small">
									查找
									<i class="icon-search icon-on-right bigger-110"></i>
								</button>
								<button  type="button" class="btn btn-purple btn-small" onclick="location.href='<%=request.getContextPath() %>/manager/classes'" >
									清空
									<i class="icon-remove icon-on-right bigger-110"></i>
								</button>
							</form>
							
							<table id="sample-table-1" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th width="10%">分类编号</th>
										<th width="20%">分类名称</th>
										<th width="40%" >操作</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${grid.rows}"  var="classes"  >
									<tr>
										<td>${classes.id}</td>
										<td>${classes.name}</td>
										<td >
											<%-- <button class="btn btn-mini btn-primary" onclick="location.href='<%=request.getContextPath() %>/manager/classesnewspage?classesid=${classes.id}'" ><i class=" icon-envelope"></i>&nbsp;发布动态</button> --%>
											<button class="btn btn-mini btn-primary" onclick="location.href='<%=request.getContextPath() %>/manager/managerstudentpage?classesid=${classes.id}'" ><i class="icon-user"></i>&nbsp;管理员工</button>
											<a href="#myModal"  role="button" onclick="setvalue('${classes.id}','${classes.name}','${classes.cateId}')" class="btn  btn-mini btn-info" data-toggle="modal"><i class="icon-edit"></i>编辑</a>
											<button class="btn btn-mini btn-danger" onclick="if(window.confirm('确认删除分类${classes.name}？')==true)location.href='<%=request.getContextPath() %>/manager/deleteclasses?id=${classes.id}'"><i class="icon-remove"></i>删除</button>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
					 		<div class="dataTables_paginate paging_bootstrap pagination">
							  <c:if test="${grid.pages > 1}">
							  <button class="btn btn-success btn-mini" onclick="searchClass(1)">首页</button>
							  <select name="classid" id="editclassid" class="btn btn-success input-medium" onchange="searchClass(this.value)">
							  	<c:forEach  varStatus="num" begin="1" end="${grid.pages }">
									<option value="${num.index }" <c:if test="${num.index ==grid.currentPage }"> selected="selected"</c:if>>第${num.index }页</option>
								</c:forEach>
							  </select>
							  <button class="btn btn-success btn-mini" onclick="searchClass(${grid.pages })" >末页</button>
							 </c:if>
					 		</div>
							<!--PAGE CONTENT ENDS-->
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div><!--/.page-content-->
		</div><!--/.main-content-->
	</div><!--/.main-container-->
	<%@include file="/WEB-INF/views/common/js.jsp" %>
	<script type="text/javascript">
		$(function() {
			$('#modify').on('click', function() {
				if($.trim($("#editname").val())==''){
					alert('请输入分类名称！');
					return;
				}else if($.trim($("#editremark").val())==''){
					alert('请输入分类法人！');
					return;
				}else{
					$("#updateclasses").submit();
				}
			});
		});
		function setvalue(id,name,remark){
			$("#editid").val(id);
			$("#editname").val(name);
			$("#editremark").val(remark);
		}
		function searchClass(pagenum){
			$("#pagenum").val(pagenum);
			$("#searchClassForm").submit();
		}
	</script>
	</body>
</html>