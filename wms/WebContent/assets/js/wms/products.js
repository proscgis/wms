$(function() {
	// 添加、修改异步提交地址
	var url = "";
	var tables = $("#dataTable")
			.dataTable(
					{
						serverSide : true,// 分页，取数据等等的都放到服务端去
						processing : true,// 载入数据的时候是否显示“载入中”
						pageLength : 10, // 首次加载的数据条数
						ordering : false, // 排序操作在服务端进行，所以可以关了。
						info : false,
						pagingType : "full_numbers",
						autoWidth : false,
						stateSave : true,// 保持翻页状态，和comTable.fnDraw(false);结合使用
						searching : false,// 禁用datatables搜索
						dom : '<"top"i>rt<"bottom"flp><"clear">',
						ajax : {
							type : "post",
							url : webPath + "/product/getData",
							data : function(d) {
								var param = {};
								param.draw = d.draw;
								param.start = d.start;
								param.length = d.length;
								var formData = $("#queryForm").serializeArray();// 把form里面的数据序列化成数组
								formData.forEach(function(e) {
									param[e.name] = e.value;
								});
								return {
									"aoData" : JSON.stringify(param)
								};// 自定义需要传递的参数。
							},
						},
						columns : [// 对应上面thead里面的序列
								{
									"data" : null,
									"width" : "10px"
								},
								{
									"data" : 'id'
								},
								{
									"data" : 'name'
								},
								{
									"data" : 'categoryId'
								},
								{
									"data" : 'buyLimit',
									"width" : "20px"
								},
								{
									"data" : 'createTime',
									"render" : function(data, type, full,
											callback) {
										return (new Date(data))
												.Format("yyyy-MM-dd hh:mm:ss");
									}
								},
								{
									"data" : 'updateTime',
									defaultContent : "",
									"render" : function(data, type, full,
											callback) {
										return (new Date(data))
												.Format("yyyy-MM-dd hh:mm:ss");
									}
								} ],
						// 操作按钮
						columnDefs : [
								{
									targets : 0,
									defaultContent : "<input type='checkbox' name='checkList'>"
								},
								{
									targets : -1,
									defaultContent : "<div class='btn-group'>"
											+ "<button id='editRow' class='btn btn-primary btn-sm' type='button'><i class='fa fa-edit'></i></button>"
											+ "<button id='delRow' class='btn btn-primary btn-sm' type='button'><i class='fa fa-trash-o'></i></button>"
											+ "</div>"
								} ],
						language : {
							"lengthMenu" : "_MENU_ 条记录每页",
							"zeroRecords" : "没有找到记录",
							"info" : "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
							"infoEmpty" : "无记录",
							"infoFiltered" : "(从 _MAX_ 条记录过滤)",
							"paginate" : {
								"previous" : "上一页",
								"next" : "下一页"
							}
						},
						// 在每次table被draw完后回调函数
						fnDrawCallback : function() {
						}
					});
	$('#mainImgFile').ace_file_input({
		no_file : '没有文件 ...',
		btn_choose : '选择',
		btn_change : '替换',
		droppable : false,
		onchange : null,
		thumbnail : false, // | true | large
		whitelist : 'gif|png|jpg|jpeg'
	// blacklist:'exe|php'
	// onchange:''
	//
	});

	// 查询按钮
	$("#btn-query").on("click", function() {
		tables.fnDraw();// 查询后不需要保持分页状态，回首页
	});

	// 添加
	$("#btn-add").on("click", function() {
		url = webPath + "/product/add";
		$("input").val();
		$("#editModal").modal("show");
	});

	// 批量删除
	$("#btn-delAll").on("click", function() {

	});

	// 导出
	$("#btn-export").on("click", function() {
	});

	// 刷新
	$("#btn-re").on("click", function() {
		tables.fnDraw(false);// 刷新保持分页状态
	});

	// checkbox全选
	$("#checkAll").on(
			"click",
			function() {
				if ($(this).prop("checked") === true) {
					$("input[name='checkList']").prop("checked",
							$(this).prop("checked"));
					// $("#dataTable tbody tr").addClass('selected');
					$(this).hasClass('selected')
				} else {
					$("input[name='checkList']").prop("checked", false);
					$("#dataTable tbody tr").removeClass('selected');
				}
			});

	// 修改
	$("#dataTable tbody").on("click", "#editRow", function() {
		var data = tables.api().row($(this).parents("tr")).data();
		$("input[name=typeId]").val(data.typeIdStr);
		$("input[name=typeNameCn]").val(data.typeNameCn);
		$("input[name=typeNameEn]").val(data.typeNameEn);

		url = webPath + "/product/update";

		$("#editModal").modal("show");
	});

	$("#btn-submit").on("click", function() {
		$("#editForm").ajaxSubmit({
			url : url,
			dataType : 'json',
			/*data : {'mainImgFile': $("#mainImgFile").val(),"product":{
				"name" : $("#editForm #name").val(),
				"categoryId" : $("#editForm #categoryId").val(),
				"buyLimit" : $("#editForm #buyLimit").val()
			}},*/
			success : function(data) {
				if (data.result != null) {
					$.each(data.result, function(i, value) {
						alert(value);
					});
				}
				bootbox.alert("保存成功", function() {
				})
			},
			error : function() {
				bootbox.alert("保存失败", function() {
				})
			}
		});

	});

	// 删除
	$("#dataTable tbody").on("click", "#delRow", function() {
		var data = tables.api().row($(this).parents("tr")).data();
		if (confirm("是否确认删除这条信息?")) {
			$.ajax({
				url : '<%=path%>/product/del/' + data.typeIdStr,
				type : 'delete',
				dataType : "json",
				cache : "false",
				success : function(data) {
					if (data.status == 1) {
						showSuccess("删除成功");
						tables.api().row().remove().draw(false);
					} else {
						showFail("删除失败");
					}
				},
				error : function(err) {
					showFail("Server Connection Error...");
				}
			});
		}
	});
});